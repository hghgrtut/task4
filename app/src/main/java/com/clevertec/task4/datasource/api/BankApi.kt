package com.clevertec.task4.datasource.api

import com.clevertec.task4.data.ApiATM
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface BankApi {
    @GET("atm")
    suspend fun getATMs(@Query(value = "city") city: String = "Гомель"): Response<List<ApiATM>>
}