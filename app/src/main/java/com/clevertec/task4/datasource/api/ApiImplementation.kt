package com.clevertec.task4.datasource.api

import android.util.Log
import com.clevertec.task4.data.ATM
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object ApiImplementation {
    private val baseUrl = "https://belarusbank.by/api/"
    private val service = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create())
        .baseUrl(baseUrl)
        .build()
        .create(BankApi::class.java)

    suspend fun getATMs(): List<ATM> {
        val response = service.getATMs()
        Log.d("TRTR", response.message())
        return if (response.isSuccessful) response.body()!!.map { ATM(it) } else emptyList()
    }
}