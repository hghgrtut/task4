package com.clevertec.task4.datasource

import com.clevertec.task4.app.ServiceLocator
import com.clevertec.task4.data.ATM
import com.clevertec.task4.datasource.api.ApiImplementation
import com.clevertec.task4.datasource.database.ATMDao
import java.net.UnknownHostException

class Repository {
    suspend fun getATMs() : List<ATM> {
        val dao: ATMDao = ServiceLocator.get(ATMDao::class)
        try {
            val atms = ApiImplementation.getATMs()
            dao.dropTable()
            dao.insertAll(atms)
            return atms
        } catch (e: UnknownHostException) {
            return dao.getATMs()
        }
    }
}