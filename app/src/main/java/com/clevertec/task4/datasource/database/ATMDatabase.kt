package com.clevertec.task4.datasource.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.clevertec.task4.data.ATM

@Database(entities = [ATM::class], version = 1)
abstract class ATMDatabase : RoomDatabase() {
    abstract fun atmDao(): ATMDao

    companion object {
        @Volatile
        private var INSTANCE: ATMDatabase? = null

        fun getDatabase(applicationContext: Context): ATMDatabase = INSTANCE ?: synchronized(this) {
            INSTANCE =
                Room.databaseBuilder(applicationContext, ATMDatabase::class.java, "ATMDatabase")
                    .fallbackToDestructiveMigration()
                    .build()
            INSTANCE!!
        }
    }
}