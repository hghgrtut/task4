package com.clevertec.task4.datasource.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.clevertec.task4.constants.TABLE_ATM
import com.clevertec.task4.data.ATM

@Dao
interface ATMDao {
    @Query("SELECT * FROM $TABLE_ATM")
    fun getATMs(): List<ATM>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(atms: List<ATM>)

    @Query("DELETE FROM $TABLE_ATM")
    fun dropTable()
}