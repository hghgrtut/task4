package com.clevertec.task4.app

import kotlin.reflect.KClass

object ServiceLocator {
    private val instances = mutableMapOf<KClass<*>, Any>()

    fun <T: Any> register(kClass: KClass<T>, instance: T) { instances[kClass] = instance }

    inline fun <reified T: Any> register(instance: T) { register(T::class, instance) }

    fun <T: Any> get(kClass: KClass<T>): T = instances.get(kClass) as T

    inline fun <reified T: Any> locate() = get(T::class)

    inline fun <reified T: Any> locateByLazy(): Lazy<T> = lazy { get(T::class) }
}