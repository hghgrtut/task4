package com.clevertec.task4.app

import android.app.Application
import com.clevertec.task4.datasource.database.ATMDatabase

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        ServiceLocator.register(ATMDatabase.getDatabase(applicationContext).atmDao())
    }
}