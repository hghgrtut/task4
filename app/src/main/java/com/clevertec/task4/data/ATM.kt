package com.clevertec.task4.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.clevertec.task4.constants.TABLE_ATM
import com.google.android.gms.maps.model.LatLng
import java.lang.StringBuilder

@Entity(tableName = TABLE_ATM)
data class ATM(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "place_details") val placeDetails: String,
    @ColumnInfo(name = "work_time") val workTime: String,
    @ColumnInfo(name = "latitude") val latitude: Double,
    @ColumnInfo(name = "longitude") val longitude: Double,
    @ColumnInfo(name = "atmPrinter") val atmPrinter: Boolean
) {
    constructor(apiData: ApiATM) : this(
        apiData.id,
        apiData.placeDetails,
        apiData.workTime,
        apiData.latitude.toDouble(),
        apiData.longitude.toDouble(),
        apiData.atmPrinter == "да"
    )

    override fun toString(): String {
        val textRepresentation = StringBuilder(placeDetails)
        if (workTime != "Круглосуточно") textRepresentation.append(", $workTime")
        if (!atmPrinter) textRepresentation.append(", нет чеков")
        return textRepresentation.toString()
    }

    fun location() = LatLng(latitude, longitude)
}