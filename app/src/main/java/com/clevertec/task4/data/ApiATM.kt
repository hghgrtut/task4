package com.clevertec.task4.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiATM(
    @Json(name = "id") val id: String,
    @Json(name = "install_place") val placeDetails: String,
    @Json(name = "work_time") val workTime: String,
    @Json(name = "gps_x") val latitude: String,
    @Json(name = "gps_y") val longitude: String,
    @Json(name = "ATM_printer") val atmPrinter: String
)