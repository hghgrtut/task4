package com.clevertec.task4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MarkerOptions
import com.clevertec.task4.databinding.ActivityMapsBinding
import com.clevertec.task4.datasource.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        MainScope().launch {
            val atms = withContext(Dispatchers.IO) { Repository().getATMs() }
            if (atms.isEmpty()) {
                val context = applicationContext
                Toast.makeText(context, getString(R.string.no_internet_no_cache), Toast.LENGTH_LONG)
                    .show()
            } else  {
                mMap.moveCamera(CameraUpdateFactory.zoomTo(14.0f))
                mMap.moveCamera(CameraUpdateFactory.newLatLng(atms.random().location()))
                atms.forEach {
                    mMap.addMarker(MarkerOptions().position(it.location()).title(it.toString()))
                }
            }
        }
    }
}